#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:68d31dc7ea941cb7401e35a8b7de4c9f5bf097c8 > /cache/recovery/last_install_recovery_status; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:33554432:ea040e38b9dc0cbc3babf23dab2823feffacd119 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:68d31dc7ea941cb7401e35a8b7de4c9f5bf097c8 >> /cache/recovery/last_install_recovery_status && \
      echo "Installing new recovery image: succeeded" >> /cache/recovery/last_install_recovery_status || \
      echo "Installing new recovery image: failed" >> /cache/recovery/last_install_recovery_status
else
  echo "Recovery image already installed" >> /cache/recovery/last_install_recovery_status
fi
